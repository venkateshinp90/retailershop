<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Retail Manager</title>
 <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
</head>
<body ng-app="reatailForm" ng-controller="RetailRestController">
<form  method="POST">
<div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">storeName Form</div>
        <div class="panel-body">
          <div class="col-md-12">
              <table class="table table-noborder">
                <tbody>
                  <td><div class="form-group">
                          <label for="storeName">storeName</label>
                          <input type="text" class="form-control" id="storeName" ng-model="user.storeName"  placeholder="Enter storeName" >
                          
                        </div></td>
                      <td><div class="form-group">
                          <label for="zoneName">zoneName</label>
                          <input type="text" class="form-control" id="zoneName" ng-model="user.zoneName" placeholder="Enter zoneName">
                           
                        </div></td>
                      <td><div class="form-group">
                          <label for="address">Address/location</label>
                          <input type="text" class="form-control"  ng-model="user.address" placeholder="Enter address" onFocus="initializeAutocomplete()" id="locality" >
                        </div></td>
                      <td><div class="form-group">
                         <label for="ownerName">OwnerName</label>
                          <input type="text" class="form-control" id="ownerName" ng-model="user.ownerName" placeholder="Enter ownerName" >
                        </div></td>
					</tr>
                </tbody>
              </table>
			  <div align="right">
			  <input type="submit" ng-click="save(user)" value="Save" />
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div>
<br></br>

  </div>
  </form>
  </body>
  </html>
  
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpscimbAi_faNj1kNTGVm6alw-T3BmT9Q&sensor=false&libraries=places"></script>
  <script>
  var lat;
  var lng;
  angular.module('reatailForm', [])
                .controller('RetailRestController', ['$scope', '$http', function($scope, $http) {
                	// $scope.naga = "<spring:message code="academyservices_url" />";
               	// $scope.url =$scope.naga+"usercreated";
               	 $scope.url="/create";
               	var config='contenttype';
               	
               	$scope.user = {
               			lattitude:lat,
               			langTudie:lng
               		}
                
            $scope.save = function(user) {
            	 $http.post($scope.url, JSON.stringify(user)).then(function(response) {
            	      $scope.userlist = response.data;
            	      
            	 });
            }
    }]);
  function initializeAutocomplete(){
    var input = document.getElementById('locality');
    var options = {}
    var autocomplete = new google.maps.places.Autocomplete(input, options);
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      var place = autocomplete.getPlace();
       lat = place.geometry.location.lat();
       lng = place.geometry.location.lng();
      var placeId = place.place_id;
      // to set city name, using the locality param
      var componentForm = {
        locality: 'short_name',
      };
      for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
          var val = place.address_components[i][componentForm[addressType]];
          document.getElementById("city").value = val;
        }
      }
  });
  }
 
</script>