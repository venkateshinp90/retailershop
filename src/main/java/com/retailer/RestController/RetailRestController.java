package com.retailer.RestController;

import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.retailer.Exception.NoAvailabilityException;
import com.retailer.Exception.RetailerException;
import com.retailer.entity.RestUtil;
import com.retailer.entity.RetailerShop;
import com.retailer.repo.RetailerShopRepo;

import lombok.Synchronized;

@RestController
public class RetailRestController {

	@Autowired
	RetailerShopRepo retailerRepo;

	/**
	 * Create RetailerShop
	 * 
	 * @param retailer
	 * @param bindingResult
	 * @return
	 * @throws RetailerException
	 */
	@PostMapping("/create")
	@Transactional
	public @Synchronized List<RetailerShop> create(@Valid @RequestBody RetailerShop retailer,
			BindingResult bindingResult) throws RetailerException {
		if (bindingResult.hasErrors()) {
			throw new RetailerException(HttpStatus.BAD_REQUEST, bindingResult);
		}
		retailerRepo.save(retailer);
		return retailerRepo.findAll();

	}

	/**
	 * retrun list of RetailerShop
	 * 
	 * @param latitude
	 * @param longitude
	 * @return
	 * @throws RetailerException
	 */
	@GetMapping("/list")
	public @Synchronized List<RetailerShop> grid(@RequestParam(required = false) String latitude,
			@RequestParam(required = false) String longitude) throws RetailerException {
		if (Objects.nonNull(latitude) && Objects.nonNull(longitude)) {
//			return retailerRepo.findByLatitudeAndLongitude(latitude,longitude,RestUtil.DISTANCE);
			return null;
		} else {
			throw new NoAvailabilityException(HttpStatus.BAD_REQUEST,
					RestUtil.error("Please Give latitude and longitude"));
		}

	}
}
