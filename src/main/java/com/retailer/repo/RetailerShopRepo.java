package com.retailer.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.retailer.entity.RetailerShop;

public interface RetailerShopRepo extends JpaRepository<RetailerShop, Integer> {
	
	
	@Query("FROM StoreEntity WHERE (6371 * ACOS(COS(RADIANS(:latitude)) * COS(RADIANS(latitude)) * COS(RADIANS(longitude) - RADIANS(:longitude)) + SIN(RADIANS(:latitude)) * SIN(RADIANS(latitude)))) < :distance")
	List<RetailerShop> findByLatitudeAndLongitude(@Param("latitude") String latitude,@Param("longitude") String longitude,@Param("distance") Double distance);

}
