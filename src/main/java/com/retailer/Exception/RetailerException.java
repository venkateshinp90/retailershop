package com.retailer.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
/**
 * 
 * @author pms02
 * Customize the RetailerException
 */
public class RetailerException extends Exception {

	
	private static final long serialVersionUID = -6843979139157556902L;

	private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
	private BindingResult bindingResult;
	private String message;

	public RetailerException() {

	}

	public RetailerException(HttpStatus httpStatus, BindingResult bindingResult) {
		if (httpStatus != null) {
			this.httpStatus = httpStatus;
		}
		this.bindingResult = bindingResult;
	}

	public RetailerException(HttpStatus httpStatus, String message) {
		if (httpStatus != null) {
			this.httpStatus = httpStatus;
		}
		this.message = message;
	}

	public RetailerException(String message, HttpStatus code) {
		super(message);
		this.httpStatus = code;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public BindingResult getBindingResult() {
		return bindingResult;
	}

}
