
package com.retailer.Exception;

import java.util.Map;

import org.springframework.http.HttpStatus;
/**
 * 
 * @author pms02
 * Customize NoAvailabilityException for inputs
 */
public class NoAvailabilityException extends RuntimeException {

	private HttpStatus status;
	private Map<String, Object> response;

	public NoAvailabilityException(HttpStatus status, Map<String, Object> response) {
		this.status = status;
		this.response = response;
		this.response.put("status", status.toString());
	}

	public HttpStatus getStatus() {
		return status;
	}

	public Map<String, Object> getResponse() {
		return response;
	}
}
