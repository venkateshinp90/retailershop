package com.retailer.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedBy;

import lombok.Data;

@Entity
@Table
@Data
public class RetailerShop {
	@Id
	@GeneratedValue
	private Integer id;
	
	@NotEmpty(message = "{NotEmpty.Retailer.storeName}")
	@Size(max = 75,message = "{Size.Retailer.storeName}")
	@Column(name = "storeName", nullable = false)
	private String storeName;
	
	@NotEmpty(message = "{NotEmpty.Retailer.zoneName}")
	@Size(max = 75,message = "{Size.Retailer.zoneName}")
	@Column(name = "zoneName", nullable = false)
	private String zoneName;
	
	@NotEmpty(message = "{NotEmpty.Retailer.address}")
	@Size(max = 200,message = "{Size.Retailer.address}")
	@Column(name = "address", nullable = false)
	private String address;
	
	@NotEmpty(message = "{NotEmpty.Retailer.address}")
	@Size(max = 200,message = "{Size.Retailer.address}")
	@Column(name = "ownerName", nullable = false)
	private String ownerName;
	
	private String latitude;
	
	private String longitude;
	
	
	@Column(name = "created_date",  updatable = false)
	private Date createdDate = new Date();
	
	@Column(name = "last_modified_date")
	private Date lastModifiedDate = new Date();
	
	@CreatedBy
	@Column(name = "created_by", length = 50, updatable = false)
	private String createdBy;
	
	@Column(name = "last_modified_by", length = 50)
	private String lastModifiedBy;

	@Column(name = "createdIp",  length = 50, updatable = false)
	private String createdIp;
	
	@Column(name = "updatedIp",  length = 50)
	private String updatedIp;
	
}
