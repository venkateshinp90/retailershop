package com.retailer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * 
 * @author pms02
 *  All Jsp Contoller  Calling 
 */
@Controller
public class RetailController {

	/**
	 * calling  MainPage
	 * @return
	 */
	@RequestMapping("/mainPage")
	public String login() {
		return "mainPage";
	}
	
	/**
	 * calling  createShop
	 * @return
	 */
	@RequestMapping("/createShop")
	public String createShops() {
		return "createShop";
	}
	
	/**
	 * calling  searchShop
	 * @return
	 */
	@RequestMapping("/searchShop")
	public String searchShop() {
		return "searchShop";
	}
}
