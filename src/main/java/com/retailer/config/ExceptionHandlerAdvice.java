package com.retailer.config;

import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.retailer.Exception.BeanError;
import com.retailer.Exception.RetailerException;

/**
 * 
 * @author pms02 Class Exception Congig
 */
@ControllerAdvice
public class ExceptionHandlerAdvice {

	/**
	 * Meathod RetailerException Congig
	 * 
	 * @param exception
	 * @return
	 */
	@ExceptionHandler(RetailerException.class)
	public ResponseEntity<?> handleException(RetailerException exception) {
		return ResponseEntity.status(exception.getHttpStatus())
				.body(this.processErrors(exception.getBindingResult(), exception.getHttpStatus()));
	}

	/**
	 * Meathod processErrors Congig
	 * 
	 * @param bindingResult
	 * @param status
	 * @return
	 */
	private BeanError processErrors(BindingResult bindingResult, HttpStatus status) {
		BeanError beanError = new BeanError();
		beanError.setStatus(status);
		Map<String, String> errors = beanError.getErrors();

		bindingResult.getFieldErrors().stream()
				.forEach((error) -> errors.put(error.getField(), error.getDefaultMessage()));

		return beanError;

	}
}
