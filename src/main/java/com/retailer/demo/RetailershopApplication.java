package com.retailer.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RetailershopApplication {

	public static void main(String[] args) {
		SpringApplication.run(RetailershopApplication.class, args);
	}

}
